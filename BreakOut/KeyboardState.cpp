#include "KeyboardState.h"

KeyboardState::KeyboardState(void) 
{
	for(int i = 0; i < sf::Keyboard::KeyCount; i++)
	{
		_keys[i] = sf::Keyboard::isKeyPressed(static_cast<sf::Keyboard::Key>(i));
	}
}

bool KeyboardState::IsKeyPressed(sf::Keyboard::Key key) const
{
	return _keys[key];
}

KeyboardState* KeyboardState::GetState()
{
	return new KeyboardState();
}

KeyboardState::~KeyboardState(void)
{
}
