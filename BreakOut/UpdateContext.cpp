#include "UpdateContext.h"

UpdateContext::UpdateContext(const GameTime& gameTime) :
	_gameTime(gameTime)
{
}

UpdateContext::~UpdateContext(void)
{
}

void UpdateContext::Tick()
{
	 _inputState.Tick();
}

const GameTime& UpdateContext::GetGameTime() const 
{
	return _gameTime;
}

const InputState& UpdateContext::GetInputState() const
{
	return _inputState;
}