#include "Game.h"
#include <assert.h>

#include "SFML\Graphics.hpp"
#include "SFML\Window.hpp"

Game::Game(int screenWidth, int screenHeight) : 
	_renderWindow(sf::VideoMode(screenWidth, screenHeight), "BreakOut!"),
	_updateContext(_gameTime),
	_graphicsContext(_gameTime, _renderWindow)
{
}

Game::~Game(void)
{
}

void Game::Tick()
{
	_updateContext.Tick();
	this->Update(_updateContext);
	this->Draw(_graphicsContext);
}

void Game::Run()
{
	assert(_isRunning);

	_isRunning = true;
	while(_isRunning)
	{
		_updateContext.GetGameTime();
		_gameTime.Tick();
		this->Tick();

		// Message pump
		sf::Event event;
        while (_renderWindow.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
			{
                _renderWindow.close();
				this->Exit();
			}
        }
	}
}

void Game::Exit()
{
	_isRunning = false;
}
