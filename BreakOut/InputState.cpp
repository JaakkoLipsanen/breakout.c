#include "InputState.h"

InputState::InputState(void)
{
	_previousKeyboardState = KeyboardState::GetState();
	_currentKeyboardState = KeyboardState::GetState();
}

InputState::~InputState(void)
{
}

void InputState::Tick()
{
	// Update keyboard states
	delete _previousKeyboardState;

	_previousKeyboardState = _currentKeyboardState;
	_currentKeyboardState = KeyboardState::GetState();

	// Update mouse states
	delete _previousMouseState;

	_previousMouseState = _currentMouseState;
	_currentMouseState = MouseState::GetState();
}

bool InputState::IsKeyPressed(sf::Keyboard::Key key) const
{
	return _currentKeyboardState->IsKeyPressed(key);
}

bool InputState::IsNewKeyPress(sf::Keyboard::Key key) const
{
	return _currentKeyboardState->IsKeyPressed(key) && !_previousKeyboardState->IsKeyPressed(key);
}

bool InputState::IsMouseButtonPressed(sf::Mouse::Button button) const
{
	return _currentMouseState->IsButtonPressed(button);
}

bool InputState::IsNewMouseButtonPress(sf::Mouse::Button button) const
{
	return _currentMouseState->IsButtonPressed(button) && !_previousMouseState->IsButtonPressed(button);
}

sf::Vector2i InputState::GetMousePosition() const
{
	return _currentMouseState->GetMousePosition();
}

sf::Vector2i InputState::GetMouseDelta() const
{
	return _currentMouseState->GetMousePosition() - _previousMouseState->GetMousePosition();
}
