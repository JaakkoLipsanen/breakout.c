#include "Player.h"
#include "BreakOutGame.h"

#include <math.h>

static const sf::Vector2f Size(100, 20);
static const float Speed = BreakOutGame::ScreenWidth;

Player::Player(void) :
	_rectangleShape(Size)
{
	this->Reset();
}

Player::~Player(void)
{
}

void Player::Update(const UpdateContext& updateContext)
{
	if(updateContext.GetInputState().IsKeyPressed(sf::Keyboard::Key::Left))
	{
		_positionTopLeft.x -= Speed * updateContext.GetGameTime().GetDeltaSeconds();
	}

	if(updateContext.GetInputState().IsKeyPressed(sf::Keyboard::Key::Right))
	{
		_positionTopLeft.x += Speed * updateContext.GetGameTime().GetDeltaSeconds();
	}

	static const float BorderPadding = 8;
	_positionTopLeft.x = std::max(BorderPadding, std::min(_positionTopLeft.x, BreakOutGame::ScreenWidth - Size.x - BorderPadding ));
}

void Player::Draw(const GraphicsContext& graphicsContext)
{
	_rectangleShape.setPosition(_positionTopLeft);
	graphicsContext.GetRenderWindow().draw(_rectangleShape);
}

sf::FloatRect Player::GetPaddleArea() const
{
	return sf::FloatRect(_positionTopLeft.x, _positionTopLeft.y, Size.x, Size.y);
}

void Player::Reset()
{
	_positionTopLeft = sf::Vector2f(BreakOutGame::ScreenWidth / 2, BreakOutGame::ScreenHeight - 32);
}