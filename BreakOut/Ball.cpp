#include "Ball.h"


#include "BreakOutGame.h"
#include "Math.h"

const static sf::Vector2f Size(16, 16);

Ball::Ball(const Player& player, TileGrid& tileGrid) :
	_player(player),
	_tileGrid(tileGrid)
{
	this->Reset();
}

Ball::~Ball(void)
{
}

sf::FloatRect Ball::GetArea() const
{
	return sf::FloatRect(_positionTopLeft.x, _positionTopLeft.y, Size.x, Size.y);
}

// Yeah horrible name
bool Ball::IsDead() const
{
	return _positionTopLeft.y > BreakOutGame::ScreenHeight;
}

void Ball::Update(const UpdateContext& updateContext)
{
	_previousPosition = _positionTopLeft;
	this->UpdatePosition(updateContext);

	this->CheckCollisions();
}

void Ball::UpdatePosition(const UpdateContext& updateContext)
{
	_speed += updateContext.GetGameTime().GetDeltaSeconds() * 4;
	_positionTopLeft += _velocity * _speed * updateContext.GetGameTime().GetDeltaSeconds();
}

void Ball::CheckCollisions()
{
	this->CheckCollisionsAgainstBorders();
	this->CheckCollisionsAgainstPlayer();
	this->CheckCollisionsAgainstTileGrid();
}

void Ball::CheckCollisionsAgainstBorders()
{
	// Horizontal collisions
	if(_positionTopLeft.x < 0)
	{
		_positionTopLeft.x = -_positionTopLeft.x;
		_velocity.x *= -1;
	}
	else if(_positionTopLeft.x > BreakOutGame::ScreenWidth - Size.x)
	{
		_positionTopLeft.x -= (_positionTopLeft.x - (BreakOutGame::ScreenWidth - Size.x));
		_velocity.x *= -1;
	}

	// Vertical collisions
	if(_positionTopLeft.y < 0)
	{
		_positionTopLeft.y = -_positionTopLeft.y;
		_velocity.y *= -1;
	}
	/*
	else if(_positionTopLeft.y > BreakOutGame::ScreenHeight - Size.y)
	{
		_positionTopLeft.y -= (_positionTopLeft.y - (BreakOutGame::ScreenHeight - Size.y));
		_velocity.y *= -1;
	}
	*/
}

void Ball::CheckCollisionsAgainstPlayer()
{
	// Check collisions only when velocity y is positive. 
	// TODO: Check if in the last frame the ball was on top of the paddle. If it was, then paddle 
	// "pushed" the ball and only x-axis should be changed (and even that's not necessary)
	if(_velocity.y > 0)
	{
		if(_player.GetPaddleArea().intersects(this->GetArea()))
		{

			if(_previousPosition.y + Size.y < _player.GetPaddleArea().top)
			{
				_velocity.y *= -1;

				float angle = atan2(_velocity.y, _velocity.x);
				float angleL = atan2(0, -1);
				float angleU = atan2(1, 1.1f);
				float angleR = atan2(0, 1);
				angle = angle;

				float centerX = _positionTopLeft.x + Size.x * 0.5f;

				float paddleWidth = _player.GetPaddleArea().width;
				float paddleCenterX = _player.GetPaddleArea().left + paddleWidth / 2;

				float ballOffsetX = paddleCenterX - centerX;
				float ballOffsetNormalizedX = ballOffsetX / (paddleWidth / 2);

				_velocity.x = -ballOffsetNormalizedX;
				_velocity = flai::normalize(_velocity);		
			}
			else
			{
				_positionTopLeft.x *= -1;
			}
		}
	}
}

void Ball::CheckCollisionsAgainstTileGrid()
{
	if(_velocity.y > 0)
	{
		return;
	}

	// Okay this is a bit stupid but whatever
	sf::FloatRect previousArea = sf::FloatRect(_previousPosition.x, _previousPosition.y, Size.x, Size.y);
	sf::FloatRect ballArea = this->GetArea();
	// --

	std::vector<sf::FloatRect>& rects = _tileGrid.GetRects();
	for(int i = 0; i < rects.size(); i++)
	{
		sf::FloatRect& rect = rects[i];
		if(ballArea.intersects(rect))
		{
			// Hits from left, velocity X changes to positive
			if(ballArea.left < rect.left + rect.width && previousArea.left >= rect.left + rect.width)
			{
				_velocity.x *= -1;
			}
			// Hits from right, velocity X changes to negative
			else if(ballArea.left + ballArea.width > rect.left && previousArea.left + previousArea.width <= rect.left)
			{
				_velocity.x *= -1;
			}
			// Hits from top, velocity Y changes to positive
			else if(ballArea.top < rect.top + rect.height && previousArea.top >= rect.top + rect.height)
			{
				_velocity.y *= -1;
			}
			// Hits from bottom, velocity Y changes to negative
			else if(ballArea.top + ballArea.height > rect.top && previousArea.top + previousArea.height <= rect.top)
			{
				_velocity.y *= -1;
			}

			rects.erase(rects.begin() + i);
			break;
		}
	}
}

void Ball::Draw(const GraphicsContext& graphicsContext)
{
	sf::RectangleShape shape(Size);
	shape.setPosition(_positionTopLeft);

	graphicsContext.GetRenderWindow().draw(shape);
}

void Ball::Reset()
{
	_positionTopLeft = sf::Vector2f(BreakOutGame::ScreenWidth / 2, BreakOutGame::ScreenHeight / 3);
	_speed = BreakOutGame::ScreenHeight / 3;
	_velocity = flai::normalize(sf::Vector2f(flai::rand(-5, 5), 10));
}