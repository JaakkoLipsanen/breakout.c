#include "GraphicsContext.h"

GraphicsContext::GraphicsContext(const GameTime& gameTime, sf::RenderWindow& renderWindow) : 
	_gameTime(gameTime),
	_renderWindow(renderWindow)
{
}

GraphicsContext::~GraphicsContext(void)
{
}

sf::RenderWindow& GraphicsContext::GetRenderWindow() const 
{
	return _renderWindow;
}

const GameTime& GraphicsContext::GetGameTime() const 
{
	return _gameTime;
}