#pragma once

#include "Game.h"
#include "Player.h"
#include "TileGrid.h"
#include "Ball.h"

class BreakOutGame : public Game
{
public:
	BreakOutGame(void);
	~BreakOutGame(void);

	static const int ScreenWidth = 640;
	static const int ScreenHeight = 720;

protected:
	virtual void Update(const UpdateContext& gameTime) override;
	virtual void Draw(const GraphicsContext& gameTime) override;

private:
	Player _player;
	TileGrid _tileGrid;
	Ball _ball;
	
	sf::Font _font;
	sf::Text _gameOverText;
	sf::Text _gameWonText;
};

