#pragma once

#include "GameTime.h"
#include "UpdateContext.h"
#include "GraphicsContext.h"

#include "SFML\Graphics.hpp"

class Game
{
public:
	Game(int screenWidth, int screenHeight);
	~Game(void);

	void Tick();
	void Run();
	void Exit();

protected:
	virtual void Update(const UpdateContext& updateContext) = 0;
	virtual void Draw(const GraphicsContext& graphicsContext) = 0;

private:
	bool _isRunning;
    GameTime _gameTime;
    UpdateContext _updateContext;
	GraphicsContext _graphicsContext;
    sf::RenderWindow _renderWindow; 
};

