#include "MouseState.h"

MouseState::MouseState(void)
{
	for(int i = 0; i < sf::Mouse::ButtonCount; i++)
	{
		_keys[i] = sf::Mouse::isButtonPressed(static_cast<sf::Mouse::Button>(i));
	}

	_mousePosition = sf::Mouse::getPosition();
}

MouseState::~MouseState(void)
{
}

bool MouseState::IsButtonPressed(sf::Mouse::Button button)
{
	return _keys[button];
}

sf::Vector2i MouseState::GetMousePosition()
{
	return _mousePosition;
}

MouseState* MouseState::GetState()
{
	return new MouseState();
}
