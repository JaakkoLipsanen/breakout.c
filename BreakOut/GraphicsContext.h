#pragma once

#include "GameTime.h"
#include "SFML\Graphics.hpp"

class GraphicsContext
{
public:
	GraphicsContext(const GameTime& gameTime, sf::RenderWindow& renderWindow);
	~GraphicsContext(void);

	const GameTime& GetGameTime() const;
    sf::RenderWindow& GetRenderWindow() const;

private:
	const GameTime& _gameTime;
    sf::RenderWindow& _renderWindow;
};
