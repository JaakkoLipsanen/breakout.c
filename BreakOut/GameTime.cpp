#include "GameTime.h"

GameTime::GameTime(void)
{
}

GameTime::~GameTime(void)
{
}

float GameTime::GetDeltaSeconds() const
{
	return _deltaTime;
}

float GameTime::GetTotalSeconds() const
{
	return _totalClock.getElapsedTime().asSeconds();
}

void GameTime::Tick()
{
	_deltaTime = _deltaClock.getElapsedTime().asSeconds();
	_deltaClock.restart();
}
