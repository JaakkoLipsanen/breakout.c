#include "TileGrid.h"

#include "Math.h"
#include "BreakOutGame.h"
#include "SFML\Graphics.hpp"

static const float Padding = 4;
static const sf::Vector2i TileCount(10, 8);
static const sf::Vector2f TileSize((BreakOutGame::ScreenWidth - TileCount.x * Padding) / TileCount.x, (BreakOutGame::ScreenHeight / 4 - TileCount.y * Padding) / TileCount.y);

TileGrid::TileGrid(void)
{
	this->Reset();
}

TileGrid::~TileGrid(void)
{
}

std::vector<sf::FloatRect>& TileGrid::GetRects()
{
	return _tileRects;
}

int TileGrid::GetTileRectCount() const
{
	return _tileRects.size();
}

void TileGrid::Draw(const GraphicsContext& graphicsContext)
{
	for(auto& rect : _tileRects)
	{
		sf::RectangleShape shape(sf::Vector2f(rect.width, rect.height));
		shape.setPosition(rect.left, rect.top);

		sf::Color color = flai::lerp(sf::Color::Red, sf::Color::Blue, rect.top / (BreakOutGame::ScreenHeight * 0.25f));
		shape.setFillColor(color);
		
		graphicsContext.GetRenderWindow().draw(shape);
	}
}

void TileGrid::Reset()
{
	_tileRects.clear();
	for(int x = 0; x < TileCount.x; x++)
	{
		for(int y = 0; y < TileCount.y; y++)
		{
			_tileRects.push_back(sf::FloatRect(Padding + x * (TileSize.x + Padding), Padding + y * (TileSize.y + Padding), TileSize.x, TileSize.y));
		}
	}
}

