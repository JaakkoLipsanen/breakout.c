#pragma once

#include "SFML\System.hpp"
#include "UpdateContext.h"
#include "GraphicsContext.h"

class Player
{
public:
	Player(void);
	~Player(void);

	sf::FloatRect GetPaddleArea() const;

	void Reset();
	void Update(const UpdateContext& updateContext);
	void Draw(const GraphicsContext& graphicsContext);

private:
	sf::Vector2f _positionTopLeft;
	sf::RectangleShape _rectangleShape;
};

