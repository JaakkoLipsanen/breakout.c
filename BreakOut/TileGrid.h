#pragma once

#include "GraphicsContext.h"
#include "SFML\Graphics.hpp"
#include <vector>

class TileGrid
{
public:
	TileGrid(void);
	~TileGrid(void);

	int GetTileRectCount() const;
	std::vector<sf::FloatRect>& GetRects();
	void RemoveRect(int index);

	void Reset();
	void Draw(const GraphicsContext& graphicsContext);

private:
	std::vector<sf::FloatRect> _tileRects;
};

