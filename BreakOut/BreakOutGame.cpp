#include "BreakOutGame.h"

BreakOutGame::BreakOutGame(void) : 
	Game(BreakOutGame::ScreenWidth, BreakOutGame::ScreenHeight),
	_ball(_player, _tileGrid)
{
	_font.loadFromFile("SegoeWP.Bold.ttf");	

	// Game over text
	_gameOverText = sf::Text("Game Over! Press Space to Restart", _font);
	_gameOverText.setPosition(BreakOutGame::ScreenWidth / 2, BreakOutGame::ScreenHeight / 2);

	sf::FloatRect gameOvertextBounds = _gameOverText.getLocalBounds();
	_gameOverText.setOrigin(gameOvertextBounds.left + gameOvertextBounds.width / 2, gameOvertextBounds.top + gameOvertextBounds.height / 2);

	// Game won text
	_gameWonText = sf::Text("Congratulations! Press Space to Restart", _font);
	_gameWonText.setPosition(BreakOutGame::ScreenWidth / 2, BreakOutGame::ScreenHeight / 2);

	sf::FloatRect gameWonTextBounds = _gameWonText.getLocalBounds();
	_gameWonText.setOrigin(gameWonTextBounds.left + gameWonTextBounds.width / 2, gameWonTextBounds.top + gameWonTextBounds.height / 2);
}

BreakOutGame::~BreakOutGame(void)
{
}

void BreakOutGame::Update(const UpdateContext& updateContext) 
{
	if(updateContext.GetInputState().IsNewKeyPress(sf::Keyboard::Key::Escape))
	{
		this->Exit();
	}
	else if(updateContext.GetInputState().IsNewKeyPress(sf::Keyboard::Key::Space))
	{
		_player.Reset();
		_ball.Reset();
		_tileGrid.Reset();
	}

	if(!_ball.IsDead() && _tileGrid.GetTileRectCount() != 0)
	{
		_player.Update(updateContext);
		_ball.Update(updateContext);
	}
}

void BreakOutGame::Draw(const GraphicsContext& graphicsContext) 
{
	// clear
	graphicsContext.GetRenderWindow().clear(sf::Color(0, 0, 0));

	// draw the game
	_player.Draw(graphicsContext);
	_tileGrid.Draw(graphicsContext);
	_ball.Draw(graphicsContext);

	if(_ball.IsDead())
	{
		graphicsContext.GetRenderWindow().draw(_gameOverText);
	}
	else if(_tileGrid.GetTileRectCount() == 0)
	{		
		graphicsContext.GetRenderWindow().draw(_gameWonText);
	}

	// display to screen
	graphicsContext.GetRenderWindow().display();
}

