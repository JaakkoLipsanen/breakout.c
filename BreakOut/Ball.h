#pragma once

#include "SFML\System.hpp"
#include "SFML\Graphics.hpp"

#include "UpdateContext.h"
#include "GraphicsContext.h"

#include "Player.h"
#include "TileGrid.h"

class Ball
{
public:
	Ball(const Player& player, TileGrid& tileGrid);
	~Ball(void);
	
	bool IsDead() const;
	sf::FloatRect GetArea() const;
	void Reset();

	void Update(const UpdateContext& updateContext);
	void Draw(const GraphicsContext& graphicsContext);

private:
	void UpdatePosition(const UpdateContext& updateContext);

	void CheckCollisions();
	void CheckCollisionsAgainstBorders();
	void CheckCollisionsAgainstPlayer();
	void CheckCollisionsAgainstTileGrid();

	sf::Vector2f _previousPosition;
	sf::Vector2f _positionTopLeft;
	sf::Vector2f _velocity;
	float _speed;

	const Player& _player;
    TileGrid& _tileGrid;
};

