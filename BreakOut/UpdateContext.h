#pragma once

#include "GameTime.h"
#include "InputState.h"

#include "SFML\System.hpp"

class UpdateContext
{
public:
	UpdateContext(const GameTime& gameTime);
	~UpdateContext(void);

	void Tick();

	const GameTime& GetGameTime() const;
	const InputState& GetInputState() const;

private:
	const GameTime& _gameTime;
    InputState _inputState;
};
