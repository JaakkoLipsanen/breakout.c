#pragma once

#include "SFML\Window.hpp"

class KeyboardState
{
public:
	~KeyboardState(void);

	bool IsKeyPressed(sf::Keyboard::Key key) const;
	static KeyboardState* GetState();

private:	
	KeyboardState(void);
    bool _keys[sf::Keyboard::Key::KeyCount];
};

