#pragma once

#include <SFML\System.hpp>

class GameTime
{
public:
	GameTime(void);
	~GameTime(void);

	void Tick();

	float GetDeltaSeconds() const;
	float GetTotalSeconds() const;

private:
	sf::Clock _deltaClock;
	sf::Clock _totalClock;
	float _deltaTime;
};

