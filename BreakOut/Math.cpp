#include "Math.h"

template<class T>
T flai::lerp(T start, T end, T amount)
{
	return start + amount * (end-start);
}