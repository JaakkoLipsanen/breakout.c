#pragma once

#include "KeyboardState.h"
#include "MouseState.h"

#include "SFML\System.hpp"
#include "SFML\Window.hpp"

#include <memory>

class InputState
{
public:
	InputState(void);
	~InputState(void);

	void Tick();
	
	bool IsKeyPressed(sf::Keyboard::Key key) const;
	bool IsNewKeyPress(sf::Keyboard::Key key) const;

	bool IsMouseButtonPressed(sf::Mouse::Button button) const;
	bool IsNewMouseButtonPress(sf::Mouse::Button button) const;

	sf::Vector2i GetMousePosition() const; 
	sf::Vector2i GetMouseDelta() const;

private:
	KeyboardState* _previousKeyboardState;
	KeyboardState* _currentKeyboardState;

	MouseState* _previousMouseState;
	MouseState* _currentMouseState;
};

