#pragma once

#include "SFML\Window.hpp"

class MouseState
{
public:
	~MouseState(void);

	bool IsButtonPressed(sf::Mouse::Button button);
	sf::Vector2i GetMousePosition();

	static MouseState* GetState();

private:
	MouseState(void);

	bool _keys[sf::Mouse::Button::ButtonCount];
	sf::Vector2i _mousePosition;
};

