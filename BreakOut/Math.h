#pragma once

#include "SFML\Graphics.hpp"

namespace flai
{
	template<class T, class T2>
	inline T lerp(T start, T end, T2 amount)
	{
		return start + amount * (end-start);
	}

    inline sf::Color lerp(sf::Color start, sf::Color end, float amount)
	{
		return sf::Color(
			lerp(start.r, end.r, amount),
			lerp(start.g, end.g, amount),
			lerp(start.b, end.b, amount),
			lerp(start.a, end.a, amount));
	}

    inline sf::Vector2f normalize(const sf::Vector2f& source)
	{
		float length = sqrt((source.x * source.x) + (source.y * source.y));
		return length == 0 ? source : sf::Vector2f(source.x / length, source.y / length);
	}

	inline float rand(float min, float max)
	{
		// !!! RAND_MAX is 32767, which is very small!! and rand() is not very good random-function
		float normalized = (float)std::rand() / (float)RAND_MAX;
		return min + normalized * (max-min);
	}
}